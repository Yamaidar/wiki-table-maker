using System;
using System.Text;

namespace FMS.GSheets.GSheets
{
    public class Range
    {
        public string SheetName { get; set; }
        public string From { get; set; }
        public int FromNumber { get; set; }
        public string To { get; set; }
        public int ToNumber { get; set; }

        private Range() {}

        public Range(string range)
        {
            var name = "";
            if (range.Contains('!'))
            {
                var index = range.IndexOf('!');
                name = range.Substring(0, index);
                range = range.Substring(index+1, range.Length - index - 1);
            }

            var counter = 0;
            string from="", to="", fromN="", toN="";
            if (range != ""&&range.Contains(':'))
            {
                while (char.IsLetter(range[counter]))
                {
                    from += range[counter];
                    counter++;
                }
                while (char.IsDigit(range[counter]))
                {
                    fromN += range[counter];
                    counter++;
                }

                counter++;
                
                while (range.Length>counter&&char.IsLetter(range[counter]))
                {
                    to += range[counter];
                    counter++;
                }
                while (range.Length>counter&&char.IsDigit(range[counter]))
                {
                    toN += range[counter];
                    counter++;
                }
            }

            if (name != "") SheetName = name;
            if (from != "") From = from;
            if (to != "") To = to;
            if (fromN != "") FromNumber = Convert.ToInt32(fromN);
            if (toN != "") ToNumber = Convert.ToInt32(toN);            
        }
        
        public override string ToString()
        {
            var s = new StringBuilder();
            s.Append(SheetName);
            var letters = From != null && To != null;
            var numbers = FromNumber != 0 && ToNumber != 0;

            if (letters || numbers)
            {
                s.Append("!");

                if (letters) s.Append(From);
                if (FromNumber!=0) s.Append(FromNumber);
                s.Append(":");
                if (letters) s.Append(To);
                if (numbers) s.Append(ToNumber);
            }

            return s.ToString();
        }
    }
}