namespace FMS.GSheets.GSheets
{
    public class SheetServiceData
    {
        public string ServiceAccountCertificatePath { get; set; }
        public string ServiceAccountEmail { get; set; }
        public string ServiceAccountCertificatePassword { get; set; }
    }
}