﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using FMS.Domain;
using FMS.GSheets.GSheets;
using Microsoft.Extensions.Configuration;
using PDWiki.WikiBuilder;

namespace PDWiki
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var builder = new ConfigurationBuilder()
                .AddJsonFile($"appsettings.json", true, true);
            var configuration = builder.Build();

            var sheetServiceData = configuration.GetSection("SheetsService").Get<SheetServiceData>();
            var sheetData = configuration.GetSection("SheetData").Get<IEnumerable<SheetData>>();
            
            SheetApi api = new SheetApi(sheetServiceData);
            
            var directory = new DirectoryCreator();
            
            var wikiBuilder = new WikiBuilder.WikiBuilder(api, directory);
            
            Console.WriteLine($"===============================================\n---------------START--------------\n===============================================\n\n");

            var result = await wikiBuilder.Build(sheetData);
            
            if(!result.IsSuccessfull) 
                Console.WriteLine($"\n---------------ERROR--------------\n\n{result.ErrorMessage}\n\n---------------ERROR--------------");

            Console.WriteLine("Press any key to continue...");
            Console.ReadLine();
        }
    }
}