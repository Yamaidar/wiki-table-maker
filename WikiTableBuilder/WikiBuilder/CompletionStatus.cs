namespace PDWiki.WikiBuilder
{
    public class CompletionStatus
    {
        public bool IsSuccessfull { get; set; }
        public string ErrorMessage { get; set; }

        public CompletionStatus()
        {
            IsSuccessfull = true;
        }
        public CompletionStatus(string errorMessage)
        {
            IsSuccessfull = false;
            ErrorMessage = errorMessage;
        }
    }
}