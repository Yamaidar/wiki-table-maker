using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using FMS.Domain;
using FMS.GSheets.GSheets;

namespace PDWiki.WikiBuilder
{
    public class WikiBuilder
    {
        private SheetApi _api { get; set; }
        private string _directory { get; set; }

        public WikiBuilder(SheetApi api, DirectoryCreator directory)
        {
            _api = api;
            _directory = directory.Directory;
        }

        public async Task<CompletionStatus> Build(IEnumerable<SheetData> data)
        {
            try
            {
                Console.WriteLine("Checking if the results directory exists...");
                
                var resultsPath = $"{_directory}/Results";
                if (!Directory.Exists(resultsPath))
                {
                    Directory.CreateDirectory(resultsPath);
                    Console.WriteLine("Results directory didn't exist. Created results directory");
                }
                else
                {
                    Console.WriteLine("Results directory exists");
                }
                
                Console.WriteLine("\n");

                foreach (var (sheet, sheetIndex) in data.Select((x, i) => (x, i + 1)))
                {
                    if (string.IsNullOrEmpty(sheet.SheetId))
                    {
                        Console.WriteLine($"{sheet.SheetId} is not a valid value for sheet id");
                        continue;
                    }
                    
                    Console.WriteLine($"---------------Sheet {sheet.SheetId}----------------\n");
                    
                    _api.SetId(sheet.SheetId);
                    foreach (var (table, index) in sheet.Tables.Select((x, i) => (x, i + 1)))
                    {
                        var ranges = new Dictionary<string, string>();

                        var tableData = new Dictionary<string, IEnumerable<IEnumerable<object>>>();

                        var _name = nameof(table.Name);
                        var _fileName = nameof(table.FileName);
                        var _columnsName = nameof(table.ColumnNames);
                        var _rowsName = nameof(table.RowNames);
                        var _valuesName = nameof(table.Values);

                        var includesRows = table.Values.IncludesRows;
                        var includesColumns = table.Values.IncludesColumns;
                        
                        Console.WriteLine($"Started getting data for table #{index}");

                        if (table.Values != null)
                        {
                            if (table.Values.Request)
                                ranges.Add(_valuesName, table.Values.Range);
                            else
                                tableData.Add(_valuesName, table.Values.Values);
                        }
                        else
                        {
                            Console.WriteLine($"No table values set up from table {sheet.SheetId}");
                            continue;
                        }

                        if (table.Name != null)
                        {
                            if (table.Name.Request)
                                ranges.Add(_name, table.Name.Range);
                            else
                                tableData.Add(_name, new List<List<object>>
                                {
                                    new List<object>{table.Name.Value}
                                });
                        }

                        if (table.FileName != null)
                        {
                            if (table.FileName.Request)
                                ranges.Add(_fileName, table.FileName.Range);
                            else
                                tableData.Add(_fileName, new List<List<object>>
                                {
                                    new List<object>(){table.FileName.Value}
                                });
                        }

                        if (table.ColumnNames != null && !includesColumns)
                        {
                            if (table.ColumnNames.Request)
                                ranges.Add(_columnsName, table.ColumnNames.Range);
                            else
                                tableData.Add(_columnsName,
                                    To2DimensionalArray(table.ColumnNames.Values, table.ColumnNames.IsVertical));
                        }

                        if (table.RowNames != null && !includesRows)
                        {
                            if (table.RowNames.Request)
                                ranges.Add(_rowsName, table.RowNames.Range);
                            else
                                tableData.Add(_rowsName,
                                    To2DimensionalArray(table.RowNames.Values, table.RowNames.IsVertical));
                        }

                        if (ranges.Count > 0)
                        {
                            var batchResult = await _api.BatchGet(ranges.Values);
                            var keys = ranges.Keys.ToArray();

                            for (var i = 0; i < batchResult.Count; i++)
                            {
                                tableData.Add(keys[i], batchResult[i].Values);
                            }
                        }
                        
                        Console.WriteLine($"Started generating wiki table #{index}");

                        var textLines = new List<string>()
                        {
                            "{| style=\"border:3px inset gray; font-size:80%; width:100%\" class=\"mw-collapsible mw-collapsed\"",
                            "|-",
                        };

                        var values_ = (List<IList<object>>) tableData[_valuesName];
                        List<List<object>> values = new List<List<object>>();

                        foreach (var value in values_)
                        {
                            values.Add(value.ToList());
                        }
                        
                        var hasRows = tableData.ContainsKey(_rowsName);
                        var hasColumns = tableData.ContainsKey(_columnsName);

                        if (hasColumns)
                        {
                            var columns = GetArrayFrom2Dimensional((List<IList<object>>) tableData[_columnsName], table.ColumnNames.IsVertical).ToList();
                            var countDiff = columns.Count - values[0].Count;

                            var diffRow = new List<string>();
                            for (int i = 0; i < Math.Abs(countDiff); i++)
                            {
                                diffRow.Add("");
                            }

                            if (countDiff < 0)
                                columns.InsertRange(0, diffRow);

                            if (countDiff > 0)
                                foreach (var row in values)
                                {
                                    row.InsertRange(0, diffRow);
                                }

                            values.Insert(0, columns);
                        }

                        if (hasRows)
                        {
                            var rows = GetArrayFrom2Dimensional((IEnumerable<IEnumerable<object>>)tableData[_rowsName], table.RowNames.IsVertical).ToList();
                            var countDiff = rows.Count - values.Count;

                            if (countDiff < 0)
                                for (int i = 0; i < Math.Abs(countDiff); i++)
                                {
                                    rows.Insert(0, "");
                                }

                            if (countDiff > 0)
                            {
                                var row = new List<object>();
                                for (int i = 0; i < rows.Count; i++)
                                    row.Add("");

                                for (int i = 0; i < Math.Abs(countDiff); i++)
                                {
                                    values.Insert(0, row);
                                }
                            }

                            for (int i = 0; i < rows.Count; i++)
                            {
                                values[i].Insert(0, rows[i]);
                            }
                        }

                        hasColumns = hasColumns || includesColumns;
                        hasRows = hasRows || includesRows;


                        if (tableData.ContainsKey(_name))
                        {
                            textLines.Add("! colspan=\"" + (values[0].Count) +
                                          "\" style=\"font-size:100%; background-color:#444444\" align=\"center\" | " +
                                          tableData[_name].First().First());
                            textLines.Add("|-");
                        }

                        for (var i = 0; i < values.Count; i++)
                        {
                            var row = values[i];
                            for (var j = 0; j < row.Count; j++)
                            {
                                var cell = row[j];

                                var styles = new List<string>();
                                var backgroundColor = hasColumns && i == 0 || hasRows && j == 0 ? "#222222" : "#444444";
                                styles.Add($"background-color:{backgroundColor}");

                                if (hasColumns && i == 0) styles.Add("font-size:80%");

                                var align = hasColumns && i > 0 || !hasColumns ? "align=\"center\"" : null;

                                var style = "style=\"" + String.Join("; ", styles) + "\"";

                                styles = new List<string>() {style};

                                if (align != null) styles.Add(align);

                                var s = String.Join(" ", styles);

                                var exclamation = i > 1 ? "|" : "!";

                                textLines.Add($"{exclamation} {s} | {cell}");
                            }

                            textLines.Add("|-");
                        }

                        textLines.Add("|}");

                        var fileName = tableData.ContainsKey(_fileName) ? tableData[_fileName].First().First() : $"File_{sheetIndex}_{index}";

                        await File.WriteAllLinesAsync($"{resultsPath}/{fileName}.txt", textLines);
                        
                        Console.WriteLine($"Wrote into file {fileName}");
                    }
                    
                    Console.WriteLine($"\n--------------End for sheet {sheet.SheetId}--------------\n");
                }
            }
            catch (Exception ex)
            {
                return new CompletionStatus(ex.Message);
            }

            return new CompletionStatus();
        }

        private IEnumerable<object> GetArrayFrom2Dimensional(IEnumerable<IEnumerable<object>> array, bool isVertical)
        {
            return isVertical ? array.Select(x => x.First()) : array.First();
        }

        private IEnumerable<IEnumerable<object>> To2DimensionalArray(IEnumerable<object> array, bool isVertical)
        {
            return isVertical
                ? array.Select(x => new List<object> {x})
                : new List<List<object>> {array.ToList()};
        }
    }
}