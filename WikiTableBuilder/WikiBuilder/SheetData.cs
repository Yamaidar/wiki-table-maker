using System.Collections.Generic;

namespace PDWiki.WikiBuilder
{
    public class SheetData
    {
        public string SheetId { get; set; }
        public IEnumerable<Table> Tables { get; set; }
    }

    public class Table
    {
        public TableData Values { get; set; }
        public ArrayData RowNames { get; set; }
        public ArrayData ColumnNames { get; set; }
        public SingleValueData Name { get; set; }
        public SingleValueData FileName { get; set; }
    }

    public class TableData
    {
        public bool Request { get; set; }
        public bool IncludesRows { get; set; }
        public bool IncludesColumns { get; set; }
        public string Range { get; set; }
        public IEnumerable<IEnumerable<string>> Values { get; set; }
    }

    public class ArrayData
    {
        public bool Request { get; set; }
        public bool IsVertical { get; set; }
        public string Range { get; set; }
        public IEnumerable<string> Values { get; set; }
    }

    public class SingleValueData
    {
        public bool Request { get; set; }
        public string Range { get; set; }
        public string Value { get; set; }
    }
}