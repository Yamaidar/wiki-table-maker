using System;

namespace FMS.Domain
{
    public class DirectoryCreator
    {
        public string Directory { get; }

        public override string ToString()
        {
            return Directory;
        }
        
        public DirectoryCreator()
        {
            var directory = AppDomain.CurrentDomain.BaseDirectory;
            var n = directory.IndexOf("\\bin");

            if(n>0)
                directory = directory.Substring(0, n);

            Directory = directory;
        }
    }
}